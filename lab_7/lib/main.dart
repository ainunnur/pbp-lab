/// Flutter code sample for AppBar

import 'package:flutter/material.dart';
import './screens/rumahsakit_screen.dart';
import './screens/dashboard_screen.dart';
import './screens/kuis_screen.dart';
import './screens/forum_screen.dart';
import './screens/login_screen.dart';

void main() => runApp(const MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'My page';

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: _title,
      home: MyDashBoard(),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class MyDashBoard extends StatelessWidget {
  const MyDashBoard({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Column(children: [
          Container(
            padding: const EdgeInsets.all(20),
            width: double.infinity,
            height: 100,
            color: Colors.blue,
            alignment: Alignment.bottomLeft,
            child: const Text(
              "Menu",
              style: TextStyle(
                color: Colors.white,
                fontSize: 20),
            ),
          ),
            const SizedBox(
              height:10,
            ),
            ListTile(
              onTap: (){
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const Dashboard(),),
                );
              },
              leading: const Icon(Icons.home, size: 20),
              title: const Text(
                'Dashboard',
                style: TextStyle(
                  fontSize: 20),
              ),
            ),
            ListTile(
              onTap: (){
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const RumahSakit(),),);
              },
              leading: const Icon(Icons.home, size: 20),
              title: const Text(
                'Rumah Sakit',
                style: TextStyle(
                  fontSize: 20),
              ),
            ),
            ListTile(
              onTap: (){
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const Forum(),),);
              },
              leading: const Icon(Icons.home, size: 20),
              title: const Text(
                'Forum',
                style: TextStyle(
                fontSize: 20),
              ),
            ),
            ListTile(
              onTap: (){
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const Kuis(),),);
              },
              leading: const Icon(Icons.home, size: 20),
              title: const Text(
                'Kuis',
                style: TextStyle(
                fontSize: 20),
              ),
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title : const Text('Dashboard'),
        actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Login()),
              );
              },
              child: const Text(
                'Login',
                style: TextStyle(
                  color: Colors.white,
                fontSize: 20,
                ),
              ),
            )
          ],
        ),
       
      body: const Center(
        child: Text(
          "Data Covid-19 Indonesia Terkini",
          style: TextStyle(
            fontSize: 30,
          ),
        ),
      ),
    );
  }
}
