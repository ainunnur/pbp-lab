1. Apakah perbedaan antara JSON dan XML?
JSON (JavaScript Object Nation) adalah syntax untuk menyimpan dan menukar data berbasis JavaScript yang mudah untuk dimengerti dan digenerate.
XML (Extensible Markup Language) adalah bahasa markup yang berfokus untuk menyimpan dan menukar data.

Berikut perbedaan JSON dan XML
XML :
    - adalah bahasa markup
    - tidak mendukung penggunaan array. XML membuat array dengan menambahkan setiap tag pada elemen. 
    - menggunakan tag pembuka dan penutup
    - lebih sulit dibaca daripada JSON karena adanya tag
    - XML menyimpan data dengan struktur pohon
    - mendukung adanya namespace dan comment
    - support banyak varian encoding
    - lebih secured daripada JSON
    - ukuran file besar sehingga transmisi data lambat
    - tag di XML menyebabkan banyak bandwith dikonsumsi secara tidak perlu sehingga pemrosesan data lebih lambat.
JSON : 
    - berbasis pada bahasa pemograman JavaScript
    - mendukung penggunaan array
    - tidak menggunakan tag
    - lebih mudah dibaca daripada XML dan terlihat sederhana
    - JSON menyimpan data dengan key dan value
    - tidak mendukung namespace dan comment
    - hanya supports UTF-8 Encoding
    - JSON lebih rentan
    - ukuran file kecil sehingga perpindahan data lebih cepat
    - memproses data secara serial sehingga pemrosesannya lebih cepat. Data juga dapat dengan mudah dimanipulasi dengan method eval().

2. Apakah perbedaan antara HTML dan XML?
HTML (Hyper Text Markup Languague) adalah bahasa markup untuk mendesain dokumen yang akan ditampilkan di web browser.

Berikut perbedaan HTML dan XML:
HTML : 
    - HTML berfokus pada menampilkan data
    - bersifat statis karena hanya untuk menampilkan data
    - tidak case sensitif (uppercase lowercase tidak diperhatikan)
    - tag penutup tidak terlalu diperhatikan
    - kesalahan kecil tidak terlalu berakibat dab dapat diabaikan (hasil masih bisa ditampilkan)
    - white space tidak dapat dipertahankan
    - memiliki tag yang telah ditentukan

XML : 
    - XML berfokus pada menyimpan dan memindahkan data
    - bersifat dinamis karena untuk memindahkan data
    - case sensitif (uppercase lowercase diperhatikan)
    - tag penutup wajib ada
    - kesalahan code berakibat fatal (error, hasil tidak dapat ditampilkan)
    - dapat mempertahankan white space
    - dapat menentukan tag sendiri sesuai kebutuhan

Sumber : 
https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://byjus.com/free-ias-prep/difference-between-xml-and-html/
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
