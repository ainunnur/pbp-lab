from django.db import models

# Create Note models here.
class Note(models.Model):
    to = models.CharField(max_length=20)
    dari = models.CharField(max_length=20)
    title = models.CharField(max_length=30)
    message = models.CharField(max_length=100)
