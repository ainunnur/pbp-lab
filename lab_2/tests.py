from django.test import TestCase

# Create your tests here.
from django.test import Client
from django.urls import resolve
from .views import index


class Lab2UnitTest(TestCase):

    def test_notes_is_exist(self):
        response = Client().get('/lab-2/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/lab-2/')
        self.assertEqual(found.func, index)


    def test_xml_is_exist(self):
        response = Client().get('/lab-2/xml/')
        self.assertEqual(response.status_code, 200)

    def test_json_is_exist(self):
        response = Client().get('/lab-2/json/')
        self.assertEqual(response.status_code, 200)

