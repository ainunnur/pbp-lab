from django.db import models

# Create your models here.
class Friend(models.Model):
    name = models.CharField(max_length=30)
    # TODO Implement missing attributes in Friend model
    npm = models.CharField(max_length=10)
    dob = models.DateField('Date of Birth (yyyy-mm-dd)')
