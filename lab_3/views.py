from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.contrib import messages
from.models import Friend
from.forms import FriendForm

# Create your views here.
@login_required(login_url = '/admin/login/')
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)
    
@login_required(login_url = '/admin/login/')
def add_friend(request):
    context ={}
    form = FriendForm(request.POST or None)
    
    if (form.is_valid and request.method == 'POST'):
        # save the form data to model
        form.save()
        messages.success(request, 'Form has been submitted')
        return HttpResponseRedirect('/lab-3')
            #return redirect('/lab-3')
  
    context['form'] = form
    return render(request, 'lab3_form.html', context)
    
