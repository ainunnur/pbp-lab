import 'package:flutter/material.dart';

import './rumahsakit_screen.dart';
import './dashboard_screen.dart';
import './forum_screen.dart';
import './login_screen.dart';

class Kuis extends StatelessWidget {
  const Kuis({Key? key}) : super(key: key);

  @override
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Column(children: [
          Container(
            padding: const EdgeInsets.all(20),
            width: double.infinity,
            height: 100,
            color: Colors.blue,
            alignment: Alignment.bottomLeft,
            child: const Text(
              "Menu",
              style: TextStyle(
                color: Colors.white,
                fontSize: 20),
            ),
          ),
            const SizedBox(
              height:10,
            ),
            ListTile(
              onTap: (){
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const Dashboard(),),
                );
              },
              leading: const Icon(Icons.home, size: 20),
              title: const Text(
                'Dashboard',
                style: TextStyle(
                  fontSize: 20),
              ),
            ),
            ListTile(
              onTap: (){
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const RumahSakit(),),);
              },
              leading: const Icon(Icons.home, size: 20),
              title: const Text(
                'Rumah Sakit',
                style: TextStyle(
                  fontSize: 20),
              ),
            ),
            ListTile(
              onTap: (){
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const Forum(),),);
              },
              leading: const Icon(Icons.home, size: 20),
              title: const Text(
                'Forum',
                style: TextStyle(
                fontSize: 20),
              ),
            ),
            ListTile(
              onTap: (){
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const Kuis(),),);
              },
              leading: const Icon(Icons.home, size: 20),
              title: const Text(
                'Kuis',
                style: TextStyle(
                fontSize: 20),
              ),
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title : const Text('Kuis'),
        actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Login()),
              );
              },
              child: const Text(
                'Login',
                style: TextStyle(
                  color: Colors.white,
                fontSize: 20,
                ),
              ),
            )
          ],
        ),
      body: const Center(
        child: Text(
          "Kuis COVID-19",
          style: TextStyle(
            fontSize: 30,
          ),
        ),
      )
    );
  }
}