from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from lab_2.models import Note
from.forms import NoteForm


# Create your views here.
@login_required(login_url = '/admin/login/')
def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

# ada beberapa yang salah
@login_required(login_url = '/admin/login/')
def add_note(request):
    context ={}
    form = NoteForm(request.POST or None, request.FILES or None)
    if(request.method == 'POST'):
        if (form.is_valid):
            # save the form data to model
            form.save()
            return redirect('/lab-4')
            #return redirect('/lab-3')
  
    context['form'] = form
    return render(request, 'lab4_form.html', context)

@login_required(login_url = '/admin/login/')
def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)